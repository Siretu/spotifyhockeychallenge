package team3;

import hockey.api.Util;
import java.awt.Color;
import hockey.api.GoalKeeper;
import hockey.api.IPlayer;
import hockey.api.IPuck;
import hockey.api.Player;
import hockey.api.ITeam;
import java.util.ArrayList;

public class Team implements ITeam {
	protected boolean attack = false;
	protected IPlayer lastPuckHolder;
	
    // Team Short Name.  Max 4 characters.
    public String getShortName() { return "LUS"; }

    // Team Name
    public String getTeamName() { return "Lussebulle"; }

    // Team color; body color
    public Color getTeamColor() { return Color.YELLOW; }

    // Team color; helmet color.
    public Color getSecondaryTeamColor() { return Color.BLACK; }

    // The team's LUCKY NUMBER!!
    public int getLuckyNumber() { return 15; }
    
    Goalie goalie;
    ArrayList<LussePlayer> players;
    
    // Get the goal keeper of the team.
    public GoalKeeper getGoalKeeper() {
        if (goalie == null) goalie = new Goalie ("Continuum",5,this);
        return goalie;
    }
    
    public ArrayList<LussePlayer> getPlayersWithRole(Role r){
    	ArrayList<LussePlayer> pls = new ArrayList<LussePlayer>();
    	for(LussePlayer p : players){
    		if(p.getRole() == r){
    			pls.add(p);
    		}
    	}
    	return pls;
    }
    
    // Get the other five players of the team.
    public Player getPlayer(int index) {
    	return getPlayerInternal(index);
    }
    
    // Get the other five players of the team.
    public LussePlayer getPlayerInternal (int index) {
    	String[] names = {"Erik", "Sean", "Uranbaserade björnen", "Argonbaserade bävern","Kryptonitbaserade älgen","Foo"};
        if (players == null) {
            players = new ArrayList<LussePlayer>();
            for (int i=0;i<5;i++) players.add (new LussePlayer(this,names[i]));
        }
        return players.get(index-1);
    }
    
    public void update(){
    	if(goalie.getPuck().getHolder() != null){
    		lastPuckHolder = goalie.getPuck().getHolder();
    	}
    	if(lastPuckHolder != null){
	    	if(lastPuckHolder.isOpponent()){
	    		attack = false;
	    	} else {
	    		attack = true;
	    	}
    	}
        calculateRoles ();
    	for (int i=0;i<players.size();i++) {
            players.get(i).update();
        }
    }
    
    public void calculateRoles () {
        ArrayList<LussePlayer> pls = new ArrayList<LussePlayer>(players);

        IPuck puck = goalie.getPuck();
    	
        ArrayList<LussePlayer> byDistPuck = new ArrayList<LussePlayer>(players);
        ArrayList<Double> distPuck = new ArrayList<Double>();
        for (int i=0;i<players.size();i++)
            distPuck.add((double)Util.dist2(players.get(i).getX()-puck.getX(), players.get(i).getY()-puck.getY()));
        sortBy (byDistPuck, distPuck);
        
        ArrayList<LussePlayer> byDistGoal = new ArrayList<LussePlayer>(players);
        ArrayList<Double> distGoal = new ArrayList<Double>();
        for (int i=0;i<players.size();i++)
        	if(players.get(i) != goalie.getPuck().getHolder()){
        		distGoal.add((double)Util.dist2(players.get(i),BasePlayer.GOAL_POSITION));
        	}
        sortBy (byDistGoal, distGoal);
        
        for (int i=0;i<pls.size();i++) {
            pls.get(i).setRole(Role.NONE);
        }
        
        if(attack){
        	if(goalie.getPuck().isHeld() && !goalie.getPuck().getHolder().isOpponent() &&
                    goalie.getPuck().getHolder().getIndex() != 0){
        		getPlayerInternal(goalie.getPuck().getHolder().getIndex()).setRole(Role.ATTACKER);
        	} else {
                byDistPuck.get(0).setRole(Role.ATTACKER);
        	}

            assignRoles(byDistGoal,2,Role.ATTACKER);
            assignRoles(pls,100,Role.DEFENDER);
        } else {
        	assignRoles(byDistPuck,3,Role.ATTACKER);
        	assignRoles(pls,100,Role.DEFENDER);
        }
        
        
        
    }
    
    public void assignRoles (ArrayList<LussePlayer> pls, int count, Role role) {
        for (int i=0;i<pls.size() && count > 0;i++) {
            if (pls.get(i).getRole() == Role.NONE) {
                pls.get(i).setRole(role);
                count--;
            }
        }
    }
    

    
    public void sortBy (ArrayList<LussePlayer> pls, ArrayList<Double> sort) {
        boolean changed = true;
        while (changed) {
            changed = false;
            for (int i=0;i<pls.size()-1;i++) {
                if (sort.get(i) > sort.get(i+1)) {
                    //Swap
                    LussePlayer tmp = pls.get(i);
                    double val = sort.get(i);
                    pls.set(i, pls.get(i+1));
                    sort.set(i,sort.get(i+1));
                    
                    pls.set(i+1,tmp);
                    sort.set(i+1,val);
                    changed = true;
                }
            }
        }
    }
}
