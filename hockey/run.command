DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

cp -r ../build/classes/team3 .
java -jar hockey.jar
